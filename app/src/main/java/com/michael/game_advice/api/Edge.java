package com.michael.game_advice.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by michael on 27.05.16.
 */
public class Edge implements Serializable{

    @SerializedName("id")
    public int id;

    @SerializedName("to")
    public int to;

    @SerializedName("label")
    public String label;

    @SerializedName("delay")
    public float delay;

    @SerializedName("kind")
    public String kind = "edge";

    /*конструкторы*/
    public Edge(int id, int to, String label, float delay) {
        this.id = id;
        this.to = to;
        this.label = label;
        this.delay = delay;
    }

    public Edge(int id, int to, String label, float delay, String kind) {
        this.id = id;
        this.to = to;
        this.label = label;
        this.delay = delay;
        this.kind = kind;
    }
}

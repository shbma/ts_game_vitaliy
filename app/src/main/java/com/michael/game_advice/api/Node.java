package com.michael.game_advice.api;

import com.google.gson.annotations.SerializedName;
import com.michael.game_advice.api.APIConstants.NodeAuthor;

import java.io.Serializable;

/**
 * Created by michael on 27.05.16.
 */
public class Node implements Serializable {

    @SerializedName("id")
    public int id;

    @SerializedName("label")
    public String label;

    @SerializedName("type")
    public int type;

    @SerializedName("kind")
    public String kind = "node";

    public NodeAuthor author;

    /*констуркторы*/
    public Node(int id, String label, int type, String kind, NodeAuthor author) {
        this.id = id;
        this.label = label;
        this.type = type;
        this.kind = kind;
        this.author = author;
    }

}

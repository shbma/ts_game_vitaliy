package com.michael.game_advice.api;

/**
 * Created by michael on 29.05.16.
 */
public class APIConstants {
    public static final String BASE_URL = "http://server.titansoft.ru/x/gm/";
    //public static final String BASE_URL2 = "http://server.titansoft.ru/x/gm/res/test.php";
    public static final String BASE_URL2 = "http://server.titansoft.ru/x/gm/index2.php";

    public static enum NodeAuthor {
        EXPERT, QUESTIONER
    }
}

package com.michael.game_advice.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by michael on 27.05.16.
 */
public class State implements Serializable{

    @SerializedName("node")
    public Node node;

    @SerializedName("edges")
    public Edge edges[];

}

package com.michael.game_advice.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.michael.game_advice.R;
import com.michael.game_advice.api.Edge;

import java.util.ArrayList;

/**
 * Created by michael on 03.06.16.
 */
public class EdgesListViewAdapter  extends BaseAdapter {
    ArrayList<Edge> items;
    Context context;
    LayoutInflater inflater;

    public EdgesListViewAdapter(ArrayList<Edge> items, Context context) {
        this.items = items;
        this.context = context;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = inflater.inflate(R.layout.list_item_edge, parent, false);
        }

        TextView itemText = (TextView) view.findViewById(R.id.edge_item_view);
        itemText.setText(items.get(position).label);

        return view;
    }

    /**
     * @param position
     * @return id узла, на которое указывает ребро
     */
    public Object getItemTargetId(int position) {
        return items.get(position).to;
    }

    /**
     * возвращает значение поля kind
     * @param position
     * @return всегда строка "edge", т.к. мы имеем дело со списком ребер
     */
    public Object getItemKind(int position) {
        return items.get(position).kind;
    }

    /**
     * @param position
     * @return label строка с текстом ребра
     */
    public Object getItemLabel(int position) {
        return items.get(position).label;
    }

    public void clear(){
        items.clear();
    }

    public void add(Edge item){
        items.add(item);
    }
}

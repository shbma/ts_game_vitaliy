package com.michael.game_advice;

import android.util.Log;
import android.widget.Toast;

import com.michael.game_advice.api.Node;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.File;
import java.util.ArrayList;

/**
 * Манипулирует историей, сохраняет, читает, редактирует и пр.
 * Created by michael on 07.06.16.
 */
public class Historic {
    private HistoryData history;

    private String rowFileName = "/history.txt";
    private String fileName;


    public Historic(String basePath) {
        fileName = basePath + rowFileName;
        //Log2.i(AdviceActivity.TAG, "fileName = " + fileName);

        history = new HistoryData();
        history = getHistory();
    }

    /**
     * Считываем сохраненную историю
     */
    public void readHistory(){

        history.setNodes(new ArrayList<Node>());
        history.setFutureNodeId(-1);

        try {
            File histFile = new File(fileName);
            if (!histFile.exists()){
                histFile.createNewFile();
                //Log2.i(AdviceActivity.TAG,"file was created");
            } else {
                //Log2.i(AdviceActivity.TAG,"file exists");
            }
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            history = (HistoryData) ois.readObject();
            ois.close();
            fis.close();
        } catch(IOException ioe){
            ioe.printStackTrace();
            return;
        } catch(ClassNotFoundException c){
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }

    }


    /**
     * Очищаем историю для новой игры
     */
    public void clearHistory(){
        history = new HistoryData(new ArrayList<Node>(), -1);

        writeHistory();
    }

    /**
     * сериализует объект с историей и сохранияет в файл
     */
    private void writeHistory() {
        try{
            FileOutputStream fos= new FileOutputStream(fileName);
            ObjectOutputStream oos= new ObjectOutputStream(fos);
            oos.writeObject(history);
            oos.close();
            fos.close();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }

    /**
     * добавляет в конец истории новый узел
     * @param newNode - объект узла
     */
    public void appendToHistory(Node newNode){
        history.getNodes().add(newNode);
        writeHistory();
    }

    /**
     * удаляет из истории узел с заданным индексом
     * @param pos - позиция удаляемого элемента
     */
    public void removeFromHistory(int pos){
        history.getNodes().remove(pos);
        writeHistory();
    }

    /**
     * запоминает в историю id узла для показа на следующем шаге
     * @param id - id узла
     */
    public void setFutureNodeId(int id){
        history.setFutureNodeId(id);
        writeHistory();
    }

    /**
     * читает из файла текущую историю
     * @return объект истории
     */
    public HistoryData getHistory(){
        readHistory();
        return history;
    }

}

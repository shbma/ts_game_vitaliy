package com.michael.game_advice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private TextView mainHeaderText;
    private TextView subHeaderText;
    private TextView announceText;
    private Button bStartGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        setButtonBehaviour();
    }

    /**
     * Подцепляем элементы разметки
     */
    private void initViews(){
        mainHeaderText = (TextView) findViewById(R.id.textMainHeader);
        subHeaderText = (TextView) findViewById(R.id.textSubHeader);
        announceText = (TextView) findViewById(R.id.textAnnounce);
        bStartGame = (Button) findViewById(R.id.buttonPlay);
    }

    /**
     * установим поведение кнопки Играть - по клику идем в собственно игровое окно
     */
    private void setButtonBehaviour(){

        bStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AdviceActivity.class);
                startActivity(intent);
            }
        });
    }
}

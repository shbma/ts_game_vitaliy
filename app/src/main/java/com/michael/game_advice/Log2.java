package com.michael.game_advice;

import android.util.Log;

/**
 * отключаемый лог
 *
 * Created by michael on 09.06.16.
 */
public class Log2 {
    public static final boolean isDebug = true;

    public static void i(String tag,String info){
        if (isDebug) {
            Log.i(tag, info);
        }
    }

    public static void i(String tag, String info, Throwable th){
        if (isDebug) {
            Log.i(tag, info, th);
        }
    }
}

package com.michael.game_advice;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.michael.game_advice.api.*;
import com.michael.game_advice.adapters.EdgesListViewAdapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//import okhttp3.Call;
//import okhttp3.FormBody;
//import okhttp3.Headers;
//import okhttp3.Interceptor;
//import okhttp3.MultipartBody;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.RequestBody;
//import okhttp3.Callback;
//import okhttp3.Response;
//import okhttp3.ResponseBody;
//import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by michael on 29.05.16.
 */
public class AdviceActivity extends Activity {
    private GameAPI gameAPI;
    State state;

    private ScrollView nodesScrollView;
    private LinearLayout nodesLayout;
    private ListView edgesList;
    private Historic historic;      //класс, осуществ-й операции с историей
    private int futureNodeId = -1;   //id узла, который покажется на следующем шаге (-1 - с начала)
    private ArrayList<Edge> currentEdges;

    public static final String TAG = "_INFO_ "; //тег для выводов в консоль

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_advice);
        initViews();

        historic = new Historic(getFilesDir().getPath());

        futureNodeId = historic.getHistory().getFutureNodeId();//id узла, который сейчас надо показать

        initNet(); //настраиваем соединение

        //список узлов, хранящийся в истории
        ArrayList<Node> historyNodes = historic.getHistory().getNodes();
        int historyLength = historyNodes.size();

        //смотрим, что спросить у сервера, исходя из истории
        String stepType = "StartGame";
        int stepId = 0;
        if (historyLength > 0){
            if ( historyNodes.get(historyLength-1).id == futureNodeId) {
                Log2.i(TAG, "removing last node");
                historic.removeFromHistory(historyLength - 1);//стираем последний узел, чтобы не дублировался
            }
            stepType = "Step";
            stepId = futureNodeId;
        }

        //инициализируем список узлов и ребер, наполняем историей
        initNodesLists(historyNodes);
        initEdgesLists();
        setEdgesListHadlers();

        //поехали
        askGameState(stepType, stepId);
    }

    /**
     * Цепляемся к разметке
     */
    private void initViews(){
        nodesScrollView = (ScrollView) findViewById(R.id.nodesScroll);
        nodesLayout = (LinearLayout) findViewById(R.id.nodesListLayout);
        edgesList = (ListView) findViewById(R.id.edgesListView);
    }

    /**
     * Инициализируем список узлов историей переписки
     *
     * @param history - коллекция объектов-узлов за всю переписку
     */
    private void initNodesLists(ArrayList<Node> history){
        //чистим nodes layout
        nodesLayout.removeAllViewsInLayout();

        //наполняем из истории
        Iterator<Node> historyIterator = history.iterator();

        while (historyIterator.hasNext()){
             appendToNodesList(historyIterator.next(),false);
        }

    }

    /**
     * Инициализируем список вариантов(ребер)
     */
    private void initEdgesLists(){
        currentEdges = new ArrayList<>();

        EdgesListViewAdapter adapter = new EdgesListViewAdapter(currentEdges, this);

        edgesList.setAdapter(adapter);
    }

    /**
     * Добавляем в список и в историю еще одно сообщение
     *
     * @param newNode - объект сообщения
     */
    private void appendToNodesList(Node newNode, boolean saveToHistory){
        int layoutId;

        if (newNode.author == APIConstants.NodeAuthor.EXPERT){
            layoutId = R.layout.list_item_node_expert;
        } else {
            layoutId = R.layout.list_item_node_questioner;
        }
        View nodeView = getLayoutInflater().inflate(layoutId, null);
        TextView text = (TextView) nodeView.findViewById(R.id.node_item_view);
        text.setText(newNode.label);
        nodesLayout.addView(nodeView);

        if (saveToHistory == true){
            historic.appendToHistory(newNode);
        }
    }

    /**
     * крутим вниз при каждом новом добавлении
     */
    private void forceScrollToBottom(){
        int nodesCount = nodesScrollView.getChildCount();

        View lastChild = nodesScrollView.getChildAt(nodesCount - 1);

        int edgesListHeight = edgesList.getHeight();
        Log2.i(AdviceActivity.TAG,"edgesListHeight="+edgesListHeight);
        Log2.i(AdviceActivity.TAG,"lastChild.getBottom()="+lastChild.getBottom());
        int bottom = Math.abs(lastChild.getBottom() + nodesScrollView.getPaddingBottom() + edgesListHeight);
        Log2.i(AdviceActivity.TAG,"bottom="+bottom);

        int sy = nodesScrollView.getScrollY();
        int sh = nodesScrollView.getHeight();
        Log2.i(AdviceActivity.TAG,"sy="+sy+", sh"+sh);

        int delta = bottom - (sy + sh);

        Log2.i(AdviceActivity.TAG,"delta="+delta);

        if (nodesCount == 1){ //т.е. это самое начало
            Log2.i(AdviceActivity.TAG, "Jumping - nodesCount=1");

            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            nodesLayout.setMinimumHeight(sh);
            //nodesLayout.setMinimumHeight(pxToDp(metrics.heightPixels) - edgesListHeight);
            //nodesLayout.setMinimumHeight(edgesList.getTop() - edgesListHeight);
            //nodesLayout.setMinimumHeight(sh - edgesListHeight - edgesList.getPaddingBottom() - edgesList.getPaddingTop());

            //нужен пересчет при повороте ...

        }

        nodesScrollView.smoothScrollBy(0, delta);


    }

    /**
     * Перезаписывает список вариантов(ребер) тем, что передано в параметре
     *
     * @param newEdges - массив объетов-ребер
     */
    private void updateEdgesList(Edge[] newEdges){
        EdgesListViewAdapter adapter = (EdgesListViewAdapter) edgesList.getAdapter();

        adapter.clear();
        for (int i=0; i < newEdges.length; i++){
            adapter.add(newEdges[i]);
        }

        adapter.notifyDataSetChanged(); //пнём список, чтобы обновился на эране
    }

    /**
     * Инициализируем объект для общения с сервером
     */
    public void initNet(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        gameAPI = retrofit.create(GameAPI.class);
    }

    /**
     * Запрашиваем у сервера состояние с данным id
     *
     * @param jax "StartGame" - начало игры, "Step" - след.шаг
     * @param id айдишник из серверной БД
     */
    public void askGameState(String jax, int id) {
        //Log.i(TAG, "in ask Game " + jax + ", id=" + Integer.toString(id));

        if (id < 0){ //нереальный id - это сигнал Начать играть с начала
            Log2.i(TAG,"id<0 --> StartGame");
            jax =  "StartGame";
            historic.clearHistory();
            initNodesLists(historic.getHistory().getNodes());
        }
        Call<State> requestOfState = gameAPI.getState(jax, id);
        requestOfState.enqueue(new Callback<State>() {

            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                if (response.isSuccessful()) {
                    //Log2.i(TAG, "successful request");
                    state = response.body();
                    Log2.i(TAG, "successful request. state.node.label="+state.node.label);
                    appendToNodesList(new Node(
                            state.node.id,
                            state.node.label,
                            state.node.type,
                            state.node.kind,
                            APIConstants.NodeAuthor.QUESTIONER
                    ),true);
                    if (state.edges == null) { // ребер нет - это оконечный узел
                        state.edges = new Edge[1];
                        state.edges[0] = new Edge(-1, -1, getString(R.string.play_again_text), 100);
                    }
                    updateEdgesList(state.edges);
                } else {
                    appendToNodesList(new Node(1, "net error", 2, "node", APIConstants.NodeAuthor.QUESTIONER),false);
                }

                //forceScrollToBottom();
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {
                //Log.i(TAG, "FAILURE: " + t.toString());
                Toast.makeText(getApplicationContext(), "FAILURE: " + t.toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

//    /**
//     * Запрос к серверу через okHttp3 за произвольными данными
//     * Источник вдохновения -
//     * https://github.com/square/okhttp/wiki/Recipes
//     *
//     * @param data массивКарта ключ-значение для передачи на сервер
//     *
//     * Функция рабочая, но данные пока забиты в коде, а не беруться из параметра
//    */
//    public void arbitraryRequest(ArrayMap data) throws IOException {
//        client = new OkHttpClient();
//
//        //RequestBody formBody = new MultipartBody.Builder()
//        RequestBody formBody = new FormBody.Builder()
//                .add("jax", "Step")
//                .add("id", "21")
//                //.setType(MultipartBody.FORM)
//                //.addFormDataPart("StartGame", "1")
//                .build();
//        Request request = new Request.Builder()
//                //.header("Content-Type","application/x-www-form-urlencoded; charset=UTF-8")
//                .url(APIConstants.BASE_URL2)
//                .post(formBody)
//                .build();
//
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
//
//                Headers responseHeaders = response.headers();
//                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
//                    Log.i(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i));
//                }
//
//                Log.i(TAG,"body.contentLength="+response.body().contentLength());
//                Log.i(TAG,"body.string="+response.body().string());
//                Log.i(TAG,"end");
//            }
//
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Log.i(TAG, "FAILURE: " + e.toString());
//                e.printStackTrace();
//            }
//        });
//
//    }

    /**
     * Ставит обработчики собыий списка ребер
     */
    public void setEdgesListHadlers(){
        edgesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                EdgesListViewAdapter adapter = (EdgesListViewAdapter) edgesList.getAdapter();

                //добавим выбранный совет в общий список сообщений
                appendToNodesList(new Node(
                        (int) adapter.getItemId(position),
                        (String) adapter.getItemLabel(position),
                        -1, //липовый type (липовый, т.к. у самого Edge нет такого поля)
                        (String) adapter.getItemKind(position),
                        APIConstants.NodeAuthor.EXPERT
                ),true);
                futureNodeId = (int) adapter.getItemTargetId(position);
//                Toast.makeText(getApplicationContext(), Integer.toString(a),
//                        Toast.LENGTH_SHORT).show();
                historic.setFutureNodeId(futureNodeId);
                askGameState("Step", futureNodeId);

            }
        });

        edgesList.addOnLayoutChangeListener(new View.OnLayoutChangeListener(){
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                forceScrollToBottom();
            }
        });
    }

    public int dpToPx(int dp)
    {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return (int) (dp * metrics.density + 0.5f);
    }

    public int pxToDp(int px)
    {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return (int) ((px-0.5f) / metrics.density);
    }

    /**
     * вывод в консоль списка узлов истории
     * @param nodes
     */
//    public static void logHistoryNodes(ArrayList<Node> nodes){
//        Iterator<Node> nodesIterator = nodes.iterator();
//        Log2.i(TAG, "history nodes:");
//        while (nodesIterator.hasNext()){
//            Log2.i(TAG,nodesIterator.next().id + ": " + nodesIterator.next().label);
//        }
//    }

}

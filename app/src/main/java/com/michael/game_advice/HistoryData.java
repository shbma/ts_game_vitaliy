package com.michael.game_advice;

import com.michael.game_advice.api.Node;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Данные истории
 * Created by michael on 08.06.16.
 */
public class HistoryData implements Serializable {
    private ArrayList<Node> nodes;  //пройденные узлы
    private int futureNodeId;       //id узла, который будет выведен на след.шаге

    public HistoryData() {
        this.nodes = new ArrayList<Node>();
        this.futureNodeId = -1;
    }

    public HistoryData(ArrayList<Node> nodes, int futureNodeId) {
        this.nodes = nodes;
        this.futureNodeId = futureNodeId;
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public void setNodes(ArrayList<Node> nodes) {
        this.nodes = nodes;
    }

    public int getFutureNodeId() {
        return futureNodeId;
    }

    public void setFutureNodeId(int futureNodeId) {
        this.futureNodeId = futureNodeId;
    }
}
